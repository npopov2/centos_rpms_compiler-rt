#
# Build/PR gating tests for *LLVM 13*
#
# Imports and runs tests provided by Fedora LLVM git for the matching LLVM version.
#
# NOTE: *always* keep this file in sync with upstream, i.e. Fedora. Since we cannot "discover" a plan,
# we must duplicate at least some part of upstream plan setup, like `adjust` or `provision`. Not necessarily
# all steps, but if we do need some of them here, let's focus on making changes in upstream first, to preserve
# one source of truth. Once TMT learns to include whole plans, we could drop the copied content from here.
#

summary: compiler-rt tests for build/PR gating
adjust:
  - because: "Plan to be ran when either executed locally, or executed by CI system to gate a build or PR."
    when: >-
      trigger is defined
      and trigger != commit
      and trigger != build
    enabled: false

  # Unfortunately, TMT does not support more declarative approach, we need to run commands on our own.
  - because: "On RHEL, CRB must be enabled to provide rarer packages"
    when: >-
      distro == rhel-9
      or distro == rhel-8
    prepare+:
      - name: Enable CRB
        how: shell
        script: dnf config-manager --set-enabled rhel-CRB

  - because: "On CentOS, CRB must be enabled to provide rarer packages"
    when: >-
      distro == centos
    prepare+:
      - name: Enable CRB
        how: shell
        script: dnf config-manager --set-enabled crb

discover:
    - name: compiler-rt-upstream-tests
      how: fmf
      url: https://src.fedoraproject.org/tests/compiler-rt.git
      ref: main
    - name: upstream-llvm-integration-testsuite
      how: fmf
      url: https://src.fedoraproject.org/tests/llvm.git
      ref: main
      test: integration-test-suite
execute:
    how: tmt
provision:
  hardware:
    memory: ">= 4 GiB"